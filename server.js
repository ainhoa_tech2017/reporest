var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

  var path =require('path');
app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
app.get('/',function(req,res){
//  res.send('Hola mundo');
res.sendFile(path.join(__dirname,'index.html'));
});

app.post('/',function(req,res){
  res.send('Hemos recibido su petición');
});



app.get('/Clientes/:idcliente',function(req,res){
//  res.send('Hola mundo');
res.send('Cliente:'+req.params.idcliente);
});

app.get('/Clientes',function(req,res){
//  res.send('Hola mundo');
res.sendFile(path.join(__dirname,'clientes.json'));
});

app.get('/Productos',function(req,res){
//  res.send('Hola mundo');
res.send(JSON.parse('[{"name":"Coche techu","type":"Coche"},{"name":"Honda techu","type":"moto"}]'));
});

app.post('/Clientes',function(req,res){
  res.send('Cliente creado');
});
app.post('/Clientes/:idcliente',function(req,res){
//  res.send('Hola mundo');
res.send('Cliente creado:'+req.params.idcliente);
});

app.put('/Clientes',function(req,res){
  res.send('Cliente modificado');
});

app.put('/Clientes/:idcliente',function(req,res){
//  res.send('Hola mundo');
res.send('Cliente modificado:'+req.params.idcliente);
});


app.delete('/Clientes',function(req,res){
  res.send('Cliente eliminado');
});

app.delete('/Clientes/:idcliente',function(req,res){
//  res.send('Hola mundo');
res.send('Cliente eliminado:'+req.params.idcliente);
});
